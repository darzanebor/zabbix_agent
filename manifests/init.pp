class zabbix_agent (
  $server_ip = '10.10.10.1',
  $client_hostname = '0.srv.somedomain.com',
  $listen_ip = '0.0.0.0',
  $ps =    {
    'pskfilepath' => '/etc/zabbix/zabbix_agentd.psk',
    'pskidentity' => '',
    'key' => '',
  },
  $agent = true,
){
  package { 'lsb-release':
    ensure => present,
  }
  if $agent {
    class { 'zabbix::agent':
      server => $server_ip,
      serveractive => $server_ip,
      hostname => $client_hostname,
      listenip => $listen_ip,
      tlsaccept => 'psk',
      tlsconnect => 'psk',
      tlspskidentity => $ps['pskidentity'],
      tlspskfile => $ps['pskfilepath'],
    }
    file { 'key_file':
      ensure => present,
      path => $ps['pskfilepath'],
      content => $ps['key'],
    }
  }
}
