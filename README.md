class { 'zabbix_agent':
    server_ip => '10.10.10.1',
    client_hostname => '0.srv.somedomain.com',
    listen_ip => '0.0.0.0',
    ps =>    {
      'pskfilepath' => '/etc/zabbix/zabbix_agentd.psk',
      'pskidentity' => 'some_identity',
      'key' => 'some_key',
    },
    agent => true,
}
